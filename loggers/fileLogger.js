import * as log from "https://deno.land/std/log/mod.ts";
import { ensureDirSync } from "https://deno.land/std/fs/mod.ts";

const date = new Date().toISOString().slice(0,10);
ensureDirSync("./logs");

await log.setup({
    handlers: {
        console: new log.handlers.ConsoleHandler("DEBUG"),
        file: new log.handlers.FileHandler("INFO", {
            filename: "./logs/log_" + date + ".txt",
            formatter: "{datetime}, {levelName}, {msg}",
        }),
    },

    loggers: {
        default: {
        level: "DEBUG",
        handlers: ["console", "file"],
        },

        fileOnlyInfo: {
        level: "INFO",
        handlers: ["file"],
        },
    },
});
  
const logger = log.getLogger("fileOnlyInfo");
export default logger;

