import Beers from "../../../db/mongoDatabase.js";

class BeerRepo {
    create(beer) {
        try{
            return Beers.insertOne({
              name: beer.name,
              brand: beer.brand,
              is_premium: beer.is_premium,
              registration_date: beer.registration_date
            });     
          } 
          catch(e) {
            console.log(e);
          }
    }

    selectAll() {
        try{
            return Beers.find();   
          }
          catch(e) {
            console.log(e);
          }
    }

    selectById(id) {
        try{
            return Beers.findOne({_id: {"$oid": id}});   
          }
          catch(e) {
            console.log(e);
          }
    }

    update(id, beer) {
        try{
            var latestBeer = this.selectById(id);
            if(beer.name !== undefined){
                latestBeer["name"] = beer.name;
            }
            if(beer.brand !== undefined){
                latestBeer["brand"] = beer.brand;
            }
            if(beer.is_premium !== undefined){
                latestBeer["is_premium"] = beer.is_premium;
            }

            return Friend.updateOne({_id: {"$oid": id}}, {$set: latestBeer});
        }
        catch(e) {
        console.log(e);
        }
    }
}

export default new BeerRepo();