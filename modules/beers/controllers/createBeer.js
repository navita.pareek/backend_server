import { createBeer } from "../services/beerService.js";
import logger from "../../../loggers/fileLogger.js";

export default async ({ request, response }) => {
   
  logger.info(`CreateBeer entry`);

    if (!request.hasBody) {
    response.status = 400;
    response.body = { msg: "Invalid beer data" };
    logger.error(`CreateBeer called with invalid data`);
    logger.info(`CreateBeer exit with response: ` + JSON.stringify(response, ['status', 'body', 'msg']));
    return;
  }

  const {
    value: { name, brand, is_premium }
  } = await request.body();

  logger.info(`beer name: ${name}`);
  logger.info(`beer brand: ${brand}`);
  logger.info(`beer is_premium: ${is_premium}`);

  if (!name || !brand) {
    response.status = 422;
    response.body = { msg: "Incorrect beer data. Name and brand are required" };
    logger.error(`CreateBeer called with missing data`);
    logger.info(`CreateBeer exit with response: ` + JSON.stringify(response, ['status', 'body', 'msg']));
    return;
  }

  const beerId = await createBeer({ name, brand, is_premium });
  response.status = 201;
  response.body = { msg: `Beer created with Id ${beerId}`, beerId };
  logger.info(`CreateBeer exit with response: ` + JSON.stringify(response, ['status', 'body', 'msg']));
};