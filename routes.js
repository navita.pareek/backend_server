import * as log from "https://deno.land/std/log/mod.ts";

import { Router } from "https://deno.land/x/oak@v4.0.0/mod.ts";
import getBeers from "./modules/beers/controllers/getBeers.js";
import getBeerDetails from "./modules/beers/controllers/getBeerDetails.js";
import createBeer from "./modules/beers/controllers/createBeer.js";
import updateBeer from "./modules/beers/controllers/updateBeer.js";
import deleteBeer from "./modules/beers/controllers/deleteBeer.js";

const router = new Router();

router
  .get("/beers", getBeers)
  .get("/beers/:id", getBeerDetails)
  .post("/beers", createBeer)
  .put("/beers/:id", updateBeer)
  .delete("/beers/:id", deleteBeer);

log.debug("Backend Server started..");

export default router;